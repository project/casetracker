<?php

/**
 * Configures the various Case Tracker options; system_settings_form().
 */
function casetracker_settings() {
  $form = array();

  $form['casetracker_general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['casetracker_general']['casetracker_default_assign_to'] = array(
    '#type' => 'textfield',
    '#title' => t('Default assigned user'),
    '#autocomplete_path' => 'casetracker_autocomplete',
    '#required' => TRUE,
    '#default_value' => variable_get('casetracker_default_assign_to', variable_get('anonymous', t('Anonymous'))),
    '#description' => t('User to be assigned the case if one is not explicitly defined.'),
  );

  foreach (array('priority', 'status', 'type') as $state) {
    $options = casetracker_case_state_load($state);
    $temp_keys = array_keys($options);
    $form['casetracker_general']['casetracker_default_case_'. $state] = array(
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('Default case %state', array('%state' => $state)),
      '#default_value' => variable_get('casetracker_default_case_'. $state, array_shift($temp_keys)),
      '#description' => t('%state to be assigned the case if one is not explicitly defined.', array('%state' => ucfirst($state))),
    );
  };

  $node_types = node_get_types('names');

  $project_types = $node_types; unset($project_types['casetracker_basic_case']);
  $form['casetracker_general']['casetracker_project_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Project node types'),
    '#options' => $project_types,
    '#default_value' => variable_get('casetracker_project_node_types', array('casetracker_basic_project')),
    '#description' => t('Select the node types that will be considered Case Tracker projects.'),
  );

  $case_types = $node_types; unset($case_types['casetracker_basic_project']);
  $form['casetracker_general']['casetracker_case_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Case node types'),
    '#options' => $case_types,
    '#default_value' => variable_get('casetracker_case_node_types', array('casetracker_basic_case')),
    '#description' => t('Select the node types that will be considered Case Tracker cases.'),
  );

  return system_settings_form($form);
}

/**
 * Displays a form for adding or editing a case state.
 */
function casetracker_case_state_edit($form_state, $csid = NULL) {
  $case_state = isset($csid) ? casetracker_case_state_load(NULL, $csid) : NULL;

  $form = array();
  $form['case_state'] = array(
    '#type' => 'fieldset',
    '#title' => t('Case state'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['case_state']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('State name'),
    '#required' => TRUE,
    '#default_value' => isset($case_state) ? $case_state['name'] : NULL,
    '#description' => t('The name for this case state. Example: "Resolved".'),
  );
  $form['case_state']['realm'] = array(
    '#type' => 'select',
    '#title' => t('State realm'),
    '#required'  => TRUE,
    '#default_value' => isset($case_state) ? $case_state['realm'] : NULL,
    '#description' => t('The realm in which this case state will appear.'),
    '#options' => array('priority' => t('priority'), 'status' => t('status'), 'type' => t('type')),
  );
  $form['case_state']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => isset($case_state) ? $case_state['weight'] : 0,
    '#description' => t('States are ordered first by weight and then by state name.'),
  );

  if ($case_state) { 
    $form['csid'] = array(
      '#type' => 'hidden', 
      '#default_value' => $csid,
    ); 
  }
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Submit'), // this text is an easter egg.
  ); 

  return $form;
}

/**
 * Displays an administrative overview of all case states available.
 */
function casetracker_case_state_overview() {
  $rows = array(); 
  $headers = array(
    t('Name'), 
    t('Realm'), 
    array(
      'data' => t('Operations'), 
      'colspan' => 2)
    );
  foreach (array('priority', 'status', 'type') as $realm) {
    foreach (casetracker_case_state_load($realm) as $csid => $name) {
      $rows[] = array(
        t($name), 
        $realm,
        l(t('edit'), 'admin/settings/casetracker/states/edit/'. $csid),
        l(t('delete'), 'admin/settings/casetracker/states/delete/'. $csid), );
    }
  }

  return theme('table', $headers, $rows);
}

/**
 * Processes the submitted results of our case state addition or editing.
 */
function casetracker_case_state_edit_submit($form, &$form_state) {
  $case_state = array(
    'name' => $form_state['values']['name'], 
    'realm' => $form_state['values']['realm'],
    'weight' => $form_state['values']['weight'],  
  );
  $case_state['csid'] = $form_state['values']['csid'] 
    ? $form_state['values']['csid'] : 
    NULL; // add or edit, eh?
  casetracker_case_state_save($case_state);
  drupal_set_message(
    t(
      'The case state %name has been updated.', 
      array('%name' =>  $form_state['values']['name'])
    )
  );
  $form_state['redirect'] = 'admin/settings/casetracker/states';
}

/**
 * If the user has asked to delete a case state, we'll double-check.
 */
function casetracker_case_state_confirm_delete($csid = NULL) {
  $case_state = casetracker_case_state_load(NULL, $csid);
  $form['csid'] = array('#type' => 'hidden', '#default_value' => $csid, );
  $form['name'] = array('#type' => 'hidden', '#default_value' => $case_state['name'], );
  return confirm_form($form, // NP: 'Eruyt Village' from 'Final Fantasy XII: Original Soundtrack Limited Edition'.
                      t('Are you sure you want to delete the case state %name?', array('%name' => $case_state['name'])),
                      'admin/settings/casetracker/states', t('This action can not be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Ayup, the user definitely wants to delete this case state.
 */
function casetracker_case_state_confirm_delete_submit($form, &$form_state) {
  drupal_set_message(t('Deleted case state %name.', array('%name' =>  $form_state['values']['name'])));
  casetracker_case_state_delete($form_state['values']['csid']);
  return 'admin/settings/casetracker/states';
}