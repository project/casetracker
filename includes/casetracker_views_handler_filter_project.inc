<?php
/**
 * Filter to only show casetracker cases.
 */
class casetracker_views_handler_filter_project extends views_handler_filter {
  function query() {
    if ($case_types = variable_get('casetracker_project_node_types', array('casetracker_basic_project'))) {
      $placeholders = db_placeholders($case_types, 'varchar');
      $table = $this->query->ensure_table('node');
      $this->query->add_where($this->options['group'], "$table.type IN ($placeholders)", $case_types);
    }
    else {
      $this->query->add_where($this->options['group'], "FALSE");
      drupal_set_message(t('You have no node types which are acting as projects.', 'error'));
    }
  }
}
