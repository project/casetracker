<?php

/**
 * Field handler to show Selective state.
 *
 * @ingroup views_field_handlers
 */
class casetracker_views_handler_field_type_name extends views_handler_field {

  function render($values) {
    if ($values->casetracker_case_case_type_id) {
      return check_plain(casetracker_case_state_load('type', $values->casetracker_case_case_type_id));
    }
    return '';
  }
}