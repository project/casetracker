<?php

/**
 * Field handler to show Selective state.
 *
 * @ingroup views_field_handlers
 */
class casetracker_views_handler_field_priority_name extends views_handler_field {

  function render($values) {
    if ($values->casetracker_case_case_priority_id) {
      return check_plain(casetracker_case_state_load('priority', $values->casetracker_case_case_priority_id));
    }
    return '';
  }
}