<?php

/**
 * Field handler to show Selective state.
 *
 * @ingroup views_field_handlers
 */
class casetracker_views_handler_field_status_name extends views_handler_field {

  function render($values) {
    if ($values->casetracker_case_case_status_id) {
      return check_plain(casetracker_case_state_load('status', $values->casetracker_case_case_status_id));
    }
    return '';
  }
}